\documentclass{beamer}

\mode<presentation> {
\usetheme{Madrid}
}

\beamertemplatenavigationsymbolsempty

\usepackage{pacman} % Allows you to be the best presentator ever :D

\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage{subcaption}
\usepackage{mathtools}
\usepackage{xcolor}
\usepackage{bm}

%-----------------------------------------------------------------------------
%	TITLE PAGE
%-----------------------------------------------------------------------------

\title[MASL - 2019/20 - Lorenzo Palloni]{Entity Embedding vs One Hot Encoding}
\subtitle{Multivariate Analysis and Statistical Learning}
\author{Lorenzo Palloni}
\institute[]{
    University of Florence\\
    \medskip
    \textit{lorenzo.palloni@stud.unifi.it }
}
\date{\today}

\begin{document}

\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

%-----------------------------------------------------------------------------
%	PRESENTATION SLIDES
%-----------------------------------------------------------------------------
%   TABLE OF CONTENTES
%-----------------------------------------------------------------------------
%\begin{frame}
%\tableofcontents
%\end{frame}
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
\begin{frame}
\frametitle{Introduction}
    \begin{itemize}
        \item \textbf{Entity Embedding}\cite{guo} and \textbf{One Hot Encoding}
        \item are different ways to encode \textbf{categorical variables}
    \end{itemize}
    \begin{itemize}
        \item we will explore both methods in terms of
        \begin{enumerate}
            \item \textbf{predictions results}
            \item \textbf{computational cost}
        \end{enumerate}
    \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
\frametitle{One Hot Encoding}
    \begin{itemize}
        \item One Hot Encoding
        \item maps each \textbf{state} of a categorical variable
            with $K \in \mathbb{N}^+$ states
    \end{itemize}
    \begin{center}
        $x \in \Big\{\ \text{'\textcolor{red}{red}'},\ \text{'\textcolor{green}{green}'},\ \text{'\textcolor{blue}{blue}'}\ \Big\}$
    \end{center}
    \begin{itemize}
        \item in a $K$-dimensional representation
    \end{itemize}
    \begin{center}
        $x \in \Big\{\ [1,\ 0,\ 0],\ [0,\ 1,\ 0],\ [0,\ 0,\ 1]\ \Big\}$.
    \end{center}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
\frametitle{Entity Embedding}
    \begin{itemize}
        \item Entity Embedding
%        \item (during a training of a neural network model)
        \item maps each state of a categorical variable
    \end{itemize}
    \begin{center}
        $x \in \Big\{\ \text{'\textcolor{red}{red}'},\ \text{'\textcolor{green}{green}'},\ \text{'\textcolor{blue}{blue}'}\ \Big\}$
    \end{center}
    \begin{itemize}
        \item in a $D$-dimensional Euclidean space
        \item where $D \in \mathbb{N}^+$ is user-defined\footnote{$D$ might be chosen in range $[1,\ K - 1]$.}
    \end{itemize}
    \begin{center}
        $x \in \Big\{\ [0.5,\ -1.2],\ [1.3,\ 0.23],\ [0.4,\ 1.1]\ \Big\}$.
    \end{center}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
    \frametitle{Motivation}
    \begin{itemize}
        \item Let $x$ be a categorical variable with
            \begin{center}
                $\mathbf{11981}$ different states.
            \end{center}
    \end{itemize}
    \begin{itemize}
        \item One Hot Encoding representation of $x$ needs
            \begin{center}
                $\mathbf{11981}$-dimensional vectors.
            \end{center}
    \end{itemize}
    \begin{itemize}
        \item Entity Embedding representation of $x$ might be e.g.
            \begin{center}
                $\mathbf{19}$-dimensional vectors.
            \end{center}
    \end{itemize}
\medskip
    \begin{itemize}
        \item Explosions in dimensionality like this leads to
            \begin{enumerate}
                \item drop in prediction performance (overfitting);
                \item computational cost in space and time.
            \end{enumerate}
    \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
    \frametitle{Experiments - Dataset}
    \begin{itemize}
        \item Dataset take from a Kaggle competition called
        \begin{itemize}
            \item[$\rightarrow$] Categorical Feature Encoding Challenge;
        \end{itemize}
        \begin{itemize}
            \item $300k$ observations;
            \item $23$ variables (all categorical);
            \item binary problem ($y \in \{0, 1\}$).
        \end{itemize}
        \medskip
        \item Dataset divided into
        \begin{itemize}
            \item 80\% $\rightarrow$ train
            \item 20\% $\rightarrow$ test
        \end{itemize}
    \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
    \frametitle{Experiments - Neural Network hyperparameters}
    \begin{itemize}
        \item To extract the Entity Embeddings we use the following architecture:
        \begin{enumerate}
            \item input layer: concatenation of embedded features + other variables;
            \item first layer: 400 hidden units and ReLU activation;
            \item second layer: 600 hidden units and ReLU activation;
            \item output layer: logistic function.
        \end{enumerate}
        \medskip
        \item Training hyperparameters:
        \begin{itemize}
            \item number of epochs: 2
            \item number of observations per mini-batch: 32
            \item optimization algorithm: Adam\cite{adam} (default values)
        \end{itemize}
        \medskip
        \item Implementation in Tensorflow\cite{tf}.
    \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
    \frametitle{Experiments - Random Forest hyperparameters}
    \begin{itemize}
        \item \textbf{Random search} with $4$-fold \textbf{cross-validation} on:
        \begin{itemize}
            \item number of decision trees:
            \begin{itemize}
                \item 125
                \item[$\rightarrow\bullet$] 175
            \end{itemize}
            \item maximum number of features used by each tree in each split:
            \begin{itemize}
                \item[$\rightarrow\bullet$] 'sqrt'
                \item 'log2'
            \end{itemize}
            \item max depth of each tree:
            \begin{itemize}
                \item 10
                \item[$\rightarrow\bullet$] 20
                \item None
            \end{itemize}
            \item minimum number of samples needed to perform a split:
            \begin{itemize}
                \item 2
                \item[$\rightarrow\bullet$] 6
            \end{itemize}
        \end{itemize}
    \end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
    \frametitle{Experiments - Neural Network Results}
    \begin{table}
      \centering
      \begin{tabular}{lr}
        \toprule
        \multicolumn{2}{r}{AUC} \\
        \cmidrule(r){1-2}
        Train      & $0.8497$      \\
        Validation & $0.7935$     \\
          Test     & $\bm{0.7933}$ \\
        \bottomrule
      \end{tabular}
      \caption{Entity Embedding Neural Network results.}
    \end{table}
\end{frame}
%-----------------------------------------------------------------------------
%\begin{frame}
%\begin{tabular}{lrrrrr}
%\toprule
%{} &             0 &             1 &  accuracy     \\
%\midrule
%precision &      0.750514 &      0.623508 &  0.731533       \\
%recall    &      0.918996 &      0.305136 &  0.731533       \\
%f1-score  &      0.826254 &      0.409747 &  0.731533       \\
%support   &  41677        &  18323        &  60000          \\
%\bottomrule
%\end{tabular}
%\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
    \frametitle{Experiments - Random Forest Results}
    \begin{table}
      \centering
      \begin{tabular}{lr}
        \toprule
        \multicolumn{2}{r}{AUC} \\
        \cmidrule(r){1-2}
        Train   & $0.9879$        \\
        Test    & $\bm{0.6121}$ \\
        \bottomrule
      \end{tabular}
      \caption{Random Forest + Entity Embeddings results.}
    \end{table}
    \begin{table}
      \centering
      \begin{tabular}{lr}
        \toprule
        \multicolumn{2}{r}{AUC} \\
        \cmidrule(r){1-2}
        Train   & $0.6818$     \\
        Test    & $\bm{0.5640}$ \\
        \bottomrule
      \end{tabular}
        \caption{Random Forest + One Hot Encoding\footnote{Variables with max 50 states used.} results.}
    \end{table}
\end{frame}
%\begin{tabular}{lrrr}
%    \begin{center}
%        \toprule
%{} &             0 &             1 &  accuracy     \\
%\midrule
%precision &      0.750514 &      0.623508 &  0.731533       \\
%recall    &      0.918996 &      0.305136 &  0.731533       \\
%f1-score  &      0.826254 &      0.409747 &  0.731533       \\
%support   &  41677        &  18323        &  60000          \\
%\bottomrule
%\end{tabular}
%\end{frame}
%-----------------------------------------------------------------------------
\begin{frame}
\frametitle{Conclusion}
\begin{itemize}
    \item \textbf{Entity Embedding} is an useful technique to put into your \textbf{toolbox};
    \item in some situations can lead to a \textbf{crucial} saving in computational resources.
\end{itemize}
\end{frame}
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------
\begingroup
\footnotesize
\begin{frame}
\frametitle{References}
\begin{thebibliography}{99}

\bibitem{guo}{Guo, C., \& Berkhahn, F. (2016). Entity embeddings of categorical variables. arXiv preprint arXiv:1604.06737.}
\bibitem{adam}{Kingma, D. P., \& Ba, J. (2014). Adam: A method for stochastic optimization. arXiv preprint arXiv:1412.6980.}
\bibitem{tf}{Abadi, M., Barham, P., Chen, J., Chen, Z., Davis, A., Dean, J., ... \& Kudlur, M. (2016). Tensorflow: A system for large-scale machine learning. In 12th {USENIX} Symposium on Operating Systems Design and Implementation ({OSDI} 16) (pp. 265-283).}

\end{thebibliography}

\end{frame}
\endgroup
%-----------------------------------------------------------------------------

\end{document}
