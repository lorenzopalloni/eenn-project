from collections import OrderedDict
import numpy as np

import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import (
    Embedding, Input, Dense, Concatenate, Reshape
)

def make_embed_dims(orig_dims, max_dim=10):
    return [min(max_dim + int(np.round(np.log(x))), x - 1) for x in orig_dims]

def make_embed_dict(cat_names, orig_dims, embed_dims):
    assert len(embed_dims) == len(orig_dims)
    return OrderedDict(
            (x, (y, z)) for x, y, z in zip(cat_names, orig_dims, embed_dims)
    )

def get_model(cat_names, other_names, orig_dims, embed_dims=None, max_dim=10):
    if embed_dims is None:
        embed_dims = make_embed_dims(orig_dims, max_dim)
    embed_dict = make_embed_dict(cat_names, orig_dims, embed_dims)

    all_inputs = []
    all_embeddings = []

    for cat_name in cat_names:
        input_tmp = Input(shape=(1,), name='input_' + cat_name)
        orig_dim = embed_dict[cat_name][0]
        embed_dim = embed_dict[cat_name][1]
        embedding_tmp = Embedding(
                input_dim = orig_dim,
                output_dim = embed_dim,
                embeddings_initializer = 'glorot_uniform',
                input_length = 1,
                name = cat_name + '_embedded'
        )(input_tmp)
        embedding_tmp = Reshape(target_shape=(embed_dim,))(embedding_tmp)
        all_embeddings.append(embedding_tmp)
        all_inputs.append(input_tmp)

    other_inputs = Input(
            shape=(len(other_names),),
            name='other_variables',
            dtype=tf.float32
    )
    all_inputs.append(other_inputs)
    all_embeddings.append(other_inputs)

    x = Concatenate()(all_embeddings)
    x = Dense(400, activation='relu')(x)
    x = Dense(600, activation='relu')(x)
    output = Dense(1, activation='sigmoid')(x)

    return Model(all_inputs, output)

